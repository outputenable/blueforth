Implementing Forth for the BBC micro:bit/ARM Cortex-M0
======================================================

*   [The Definitive Guide to ARM Cortex-M0 and Cortex-M0+ Processors, 2nd Edition](https://www.elsevier.com/books/the-definitive-guide-to-arm-cortex-m0-and-cortex-m0-processors/yiu/978-0-12-803277-0) by Joseph Yiu

*   [BBC micro:bit](https://microbit.org/)
*   Nordic Semiconductor [nRF51822 Product Specification](https://infocenter.nordicsemi.com/topic/struct_nrf51/struct/nrf51822_ps.html)

*   [GNU Arm Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm)

*   [Debugging the BBC micro:bit with pyOCD and GDB](https://os.mbed.com/docs/mbed-os/latest/tutorials/debug-microbit.html)

*   [Measuring the BBC micro:bit LED current draw](https://www.seismicmatt.com/2019/03/06/measuring-the-bbc-microbit-led-current-draw/) by Matt Oppenheim

*   [Threaded Interpretive Languages](https://archive.org/details/R.G.LoeligerThreadedInterpretiveLanguagesTheirDesignAndImplementationByteBooks1981) by R. G. Loelinger
*   [eForth Overview](https://www.amazon.com/eForth-Overview-C-H-Juergen-Pintaske/dp/1726852369) book by C. H. Ting
    *   [eForth - a small Forth Implementation by C.-H. Ting](https://wiki.forth-ev.de/doku.php/en:projects:430eforth:start)
*   [exemark.com's FORTH](http://www.exemark.com/FORTH.htm) page

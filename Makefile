AS = arm-none-eabi-as
LD = arm-none-eabi-ld
OBJCOPY = arm-none-eabi-objcopy

ASFLAGS = -mcpu=cortex-m0 -mthumb
LD_SCRIPT = microbit.lds

all: ltbf.hex
.PHONY: all

ltbf.hex: microbit.o microbit.lds
	$(LD) -T $(LD_SCRIPT) -Map=ltbf.map -o ltbf.elf $<
	$(OBJCOPY) -O ihex ltbf.elf ltbf.hex

.PHONY: clean
clean:
	rm -f microbit.o
	rm -f ltbf.elf
	rm -f ltbf.hex
	rm -f ltbf.map

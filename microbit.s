        @
        @ Copyright (c) 2019 Oliver Ebert <oe@outputenable.net>
        @
        @ SPDX-License-Identifier: MIT
        @
        .syntax unified

        @ See Figure 5 Memory Map, 3.2 Memory, nRF51822 PS v3.3.
        .equ    SRAM_BASE, 0x20000000
        @ The micro:bit has 16 KiB of SRAM.
        .equ    SRAM_SIZE, 16 * 1024

        @ CLOCK peripheral
        @ See
        @ - Table 62: Instances, 13.2 Register overview, p. 53, nRF51
        @   Series RM, v3.0.1
        @ - Table 63: Register Overview, 13.2 Register Overview, p. 53,
        @   nRF51 Series RM, v3.0.1
        .equ    CLOCK_BASE, 0x40000000
        .equ    CLOCK_TASKS, 0x000
        .equ    CLOCK_HFCLKSTART, 0x00
        .equ    CLOCK_EVENTS, 0x100
        .equ    CLOCK_HFCLKSTARTED, 0x00

        @ GPIO Port peripheral
        @ See 14 General-Purpose Input/Output (GPIO), nRF51 Series RM
        @ v3.0.1.
        .equ    GPIO_BASE, 0x50000000
        .equ    GPIO_REGS, 0x500
        .equ    GPIO_OUTSET, 0x08
        .equ    GPIO_OUTCLR, 0x0C
        .equ    GPIO_DIRSET, 0x18
        .equ    GPIO_DIRCLR, 0x1C

        @ UART peripheral
        @ See 29 Universal Asynchronous Receiver/Transmitter (UART),
        @ nRF51 Series RM v3.0.1
        .equ    UART0_BASE, 0x40002000
        .equ    UART_TASKS, 0x000
        .equ    UART_STARTRX, 0x00
        .equ    UART_STARTTX, 0x08
        .equ    UART_EVENTS, 0x100
        .equ    UART_RXDRDY, 0x08
        .equ    UART_TXDRDY, 0x1C
        .equ    UART_REGS, 0x500
        .equ    UART_ENABLE,   0x00
        .equ    UART_PSELTXD,  0x0C
        .equ    UART_PSELRXD,  0x14
        .equ    UART_RXD,      0x18
        .equ    UART_TXD,      0x1C
        .equ    UART_BAUDRATE, 0x24
        .equ    UART_Baud9600, 0x00275000

        @ See
        @ - Schematics,
        @   https://tech.microbit.org/hardware/schematic/
        @ - micro:bit IoT In C - The LED Display,
        @   https://www.iot-programmer.com/index.php/books/27-micro-bit-iot-in-c/chapters-micro-bit-iot-in-c/54-micro-bit-iot-in-c-the-led-display
        .equ    MICROBIT_TGT_RX, 25
        .equ    MICROBIT_TGT_TX, 24
        .equ    MICROBIT_ROW1,   13
        .equ    MICROBIT_COL1,    4

        @ (Alignments are given in number of low-order zero bits.)
        .equ    CELLA, 2  @ Cell alignment
        .equ    CELLL, 4  @ Cell length

        .equ    CFAA, CELLA  @ Code field alignment (must be >= CELLA!)
        .equ    CFL,  4      @ Code field length

        @ Forth memory map
        .equ    EM,    SRAM_BASE + SRAM_SIZE  @ End of memory
        .equ    COLDD, (16 + 32) * 4          @ Cold start vector
        .equ    US,    64 * CELLL             @ User area size in cells
        .equ    UPP,   EM - US                @ Start of user area (UP0)
        .equ    RTS,   64 * CELLL             @ Return stack size
        .equ    RPP,   UPP                    @ Start of return stack (RP0)
        .equ    TIBB,  RPP - RTS              @ Terminal input buffer (TIB)
        .equ    SPP,   TIBB                   @ Start of data stack (SP0)

        @ Initial value of Main Stack Pointer (SP_main)
        .equ    MSP_INIT, RPP

        @ Forth virtual machine registers
EIP     .req    R7   @ Interpreter pointer (IP is a built-in register, R12)
RP      .req    R6   @ Return stack pointer
@SP             R13  @ Data stack pointer (built-in register)
WP      .req    R1   @ Word pointer
XP      .req    R0   @ Temporary register

        @
        @ Define a Forth primitive/code word.
        @
        @ Parameters:
        @ name - Forth name of the word.
        @ label - String to use in generated label names; defaults to
        @         name.
        @
        .macro  defcode name, label
        .byte   99f - 98f
98:     .ascii  "\name"
99:     .p2align CFAA
        .ifnb   \label
        .type   \label, %function
\label\():
        .else
        .type   \name, %function
\name\():
        .endif
        .endm

        @
        @ Define a Forth colon word.
        @
        @ Parameters:
        @ name - Forth name of the word.
        @ label - String to use in generated label names; defaults to
        @         name.
        @
        .macro  defword name, label
        .byte   99f - 98f
98:     .ascii  "\name"
99:     .p2align CFAA
        .ifnb   \label
        .type   \label, %function
\label\():
        .else
        .type   \name, %function
\name\():
        .endif
        BL      doLIST
        .p2align CELLA
        .endm

        .macro  $CELL value more:vararg
        .4byte  \value
        .ifnb   \more
        $CELL   \more
        .endif
        .endm

        .set    USER, 0

        .macro  $USER name, label
        defword \name, \label
        $CELL   doUSER USER
        .set    USER, USER + CELLL
        .endm

        @
        @ The Forth inner interpreter (DCT)
        @
        .macro  $NEXT
        LDM     EIP!, {WP}
        BX      WP
        .endm

        .text
        .org    0

        @ Vector table
        .word   MSP_INIT
        .word   orig
        .rept   14
        .word   panic
        .endr
        @ IRQ0 to IRQ31
        @ See Table 18 Peripheral instance reference, 5 Instance table,
        @ p. 37, nRF51822 PS v3.3.
        .rept   32
        .word   panic
        .endr

        .org    COLDD

        @ ColdBoot
        .type   orig, %function
orig:   LDR     RP, =RPP
        LDR     R0, =SPP
        MOV     SP, R0
        ADR     EIP, COLD + CFL
        $NEXT

        @
        @ Run address list in a colon word.
        @
        @ Parameters:
        @ WP - CFA + 1 (Thumb mode)
        @
        .type   doLIST, %function
doLIST: SUBS    RP, RP, #CELLL
        STR     EIP, [RP]
        ADDS    EIP, WP, #CFL - 1
        $NEXT

        defcode PANIC  @ ( -- )
        .type   panic, %function
panic:  B       panic

        defcode HFCLKSTART  @ ( -- )
        @
        @ Start High Frequency Clock (HFCLK)
        @
        @ See
        @ - 8.3 Block resource requirements, p. 49, nRF51822 PS v3.3
        @ - 3.6 Clock Management (CLOCK), p. 28ff., nRF51822 PS v3.3
        @ - 13 Clock Management (CLOCK), p. 51ff., nRF51 Series RM
        @   v3.0.1
        @
        @ Clear HFCLK oscillator started event
        LDR     R0, =CLOCK_BASE + CLOCK_EVENTS
        MOVS    R1, #0
        STR     R1, [R0, #CLOCK_HFCLKSTARTED]
        @ Start HFCLK crystal oscillator
        LDR     R1, =CLOCK_BASE + CLOCK_TASKS
        MOVS    R2, #1
        STR     R2, [R1, #CLOCK_HFCLKSTART]
        @ Busy-wait until HFCLK oscillator has started
1:      LDR     R1, [R0, #CLOCK_HFCLKSTARTED]
        ORRS    R1, R1, R1
        BEQ     1b
        $NEXT

        defcode !IO, BANGIO  @ ( -- )
        @
        @ Initialize UART0
        @
        @ See
        @ - 29 Universal Asynchronous Receiver/Transmitter (UART),
        @   p. 151ff., nRF51 Series RM v3.0.1
        @ - 29.2 Pin Configuration, p. 151, nRF51 Series RM v3.0.1
        @ - Table 273: GPIO configuration, p. 151, nRF51 Series RM
        @   v3.0.1
        @
        LDR     R0, =GPIO_BASE + GPIO_REGS
        MOVS    R2, #1
        LSLS    R1, R2, #MICROBIT_TGT_RX
        STR     R1, [R0, #GPIO_DIRCLR]
        LSLS    R1, R2, #MICROBIT_TGT_TX
        STR     R1, [R0, #GPIO_OUTSET]
        STR     R1, [R0, #GPIO_DIRSET]
        LDR     R0, =UART0_BASE + UART_REGS
        MOVS    R1, #MICROBIT_TGT_RX
        STR     R1, [R0, #UART_PSELRXD]
        MOVS    R1, #MICROBIT_TGT_TX
        STR     R1, [R0, #UART_PSELTXD]
        LDR     R1, =UART_Baud9600
        STR     R1, [R0, #UART_BAUDRATE]
        MOVS    R1, #4
        STR     R1, [R0, #UART_ENABLE]
        LDR     R0, =UART0_BASE + UART_TASKS
        STR     R2, [R0, #UART_STARTRX]
        STR     R2, [R0, #UART_STARTTX]
        $NEXT

        defcode ?RX, QRX  @ ( -- c T | F )
        @
        @ Receive a single byte via UART0.
        @
        @ Check Data received in RXD event
        LDR     R0, =UART0_BASE + UART_EVENTS
        LDR     R1, [R0, #UART_RXDRDY]
        SUBS    R1, R1, #1
        BNE     1f
        @ Clear Data received in RXD event
        STR     R1, [R0, #UART_RXDRDY]
        @ Read RXD register
        LDR     R0, =UART0_BASE + UART_REGS
        LDR     R2, [R0, #UART_RXD]
        PUSH    {R2}
1:      MVNS    R1, R1
        PUSH    {R1}
        $NEXT

        defcode TX!, TXBANG  @ ( c -- )
        @
        @ Send a single byte via UART0.
        @
        @ Clear Data sent from TXD event
        LDR     R0, =UART0_BASE + UART_EVENTS
        MOVS    R1, #0
        STR     R1, [R0, #UART_TXDRDY]
        @ Set TX data to be transferred
        LDR     R1, =UART0_BASE + UART_REGS
        POP     {R2}
        STR     R2, [R1, #UART_TXD]
        @ Busy-wait for Data sent from TXD event
1:      LDR     R1, [R0, #UART_TXDRDY]
        ORRS    R1, R1, R1
        BEQ     1b
        $NEXT

        .ltorg

        defcode EXIT  @ ( -- )
        LDM     RP!, {EIP}
        $NEXT

        defcode next  @ ( -- )
        @
        @ Decrement index and exit loop if index is less than zero.
        @
        LDR     WP, [RP]
        SUBS    WP, WP, #1
        BCC     1f
        STR     WP, [RP]
        LDR     EIP, [EIP]
        $NEXT
1:      ADDS    RP, RP, #CELLL
        LDM     EIP!, {XP, WP}
        BX      WP

        defcode ?branch, qbranch  @ ( f -- )
        @
        @ Branch if flag is zero.
        @
        POP     {WP}
        ORRS    WP, WP, WP
        BEQ     1f
        @ Fused ADD EIP, #CELLL/$NEXT (skipping inline branch target address)
        LDM     EIP!, {XP, WP}
        BX      WP
1:      LDR     EIP, [EIP]
        $NEXT

        defcode branch  @ ( -- )
        LDR     EIP, [EIP]
        $NEXT

        defcode 0<, ZLESS  @ ( n -- f )
        @
        @ Return whether n is negative.
        @
        LDR     WP, [SP]
        LSRS    WP, WP, #31
        RSBS    WP, WP, #0
        STR     WP, [SP]
        $NEXT

        defcode EXECUTE  @ ( ca -- )
        POP     {WP}
        BX      WP

        defcode doLIT
        LDM     EIP!, {XP, WP}
        PUSH    {XP}
        BX      WP

        defcode UM+, UMPLUS  @ ( w w -- w cy )
        POP     {XP, WP}
        ADDS    WP, WP, XP
        EORS    XP, XP, XP  @ (does not affect the carry flag)
        ADCS    XP, XP, XP
        PUSH    {XP, WP}
        $NEXT

        defcode SP!, SPSTORE  @ ( w -- )
        POP     {WP}
        MOV     SP, WP
        $NEXT

        defcode C!, CSTORE  @ ( c b -- )
        POP     {XP, WP}
        STRB    WP, [XP]
        $NEXT

        defcode C\@, CFETCH  @ ( b -- c )
        POP     {WP}
        LDRB    WP, [WP]
        PUSH    {WP}
        $NEXT

        defcode !, STORE  @ ( w a -- )
        POP     {XP, WP}
        STR     WP, [XP]
        $NEXT

        defcode \@, FETCH  @ ( a -- w )
        POP     {WP}
        LDR     WP, [WP]
        PUSH    {WP}
        $NEXT

        defcode RP!, RPSTORE  @ ( a -- )
        POP     {RP}
        $NEXT

        defcode R>, RFROM  @ ( -- w )
        LDM     RP!, {WP}
        PUSH    {WP}
        $NEXT

        defcode R\@, RFETCH  @ ( -- w )
        LDR     WP, [RP]
        PUSH    {WP}
        $NEXT

        defcode >R, TOR  @ ( w -- )
        POP     {WP}
        SUBS    RP, RP, #CELLL
        STR     WP, [RP]
        $NEXT

        defcode DROP  @ ( w -- )
        POP     {WP}
        $NEXT

        defcode DUP  @ ( w -- w w )
        LDR     WP, [SP]
        PUSH    {WP}
        $NEXT

        defcode SWAP  @ ( w1 w2 -- w2 w1 )
        POP     {XP, WP}
        PUSH    {XP}
        PUSH    {WP}
        $NEXT

        defcode OVER  @ ( w1 w2 -- w1 w2 w1 )
        LDR     WP, [SP, #CELLL]
        PUSH    {WP}
        $NEXT

        defcode XOR  @ ( w w -- w )
        POP     {XP, WP}
        EORS    WP, WP, XP
        PUSH    {WP}
        $NEXT

        defword COLD  @ ( -- )
        $CELL   doLIT uzero UP FETCH doLIT ulast-uzero CMOVE  @ Initialize user variables
        $CELL   PRESET HFCLKSTART BANGIO
        $CELL   QUIT

        defword PRESET  @ ( -- )
        $CELL   SP0 FETCH SPSTORE  doLIT TIBB NTIB CELLP STORE EXIT

        defword QUIT  @ ( -- )
        $CELL   RP0 FETCH RPSTORE
1:      $CELL   QUERY CR branch 1b

        defword ^H, BKSP  @ ( bot eot cur -- bot eot cur )
        @
        @ Erase the last character ("backspace") and decrement cur.  If
        @ cur is equal to bot then do nothing.
        @
        $CELL   TOR OVER RFROM SWAP OVER XOR
        $CELL   qbranch 1f  doLIT 8 TECHO FEXECUTE
        $CELL     BL TECHO FEXECUTE
        $CELL     doLIT 8 TECHO FEXECUTE
        $CELL     doLIT 1 MINUS
1:      $CELL   EXIT

        defword TAP  @ ( bot eot cur c -- bot eot cur )
        @
        @ Echo c, store c at cur, and bump cur.
        @
        $CELL   DUP TECHO FEXECUTE  OVER CSTORE  doLIT 1 PLUS EXIT

        defword kTAP  @ ( bot eot cur c -- bot eot cur )
        @
        @ Process (non-printable) control character c.
        @ If c is backspace (8) then ^H is called.
        @ If c is carriage return (13) then eot is set to cur.
        @ Otherwise a blank is appended to the text.
        @
        $CELL   DUP doLIT 13 XOR
        $CELL   qbranch 3f  doLIT 8 XOR
        $CELL     qbranch 1f  BL TAP branch 2f
1:      $CELL     BKSP
2:      $CELL     EXIT
3:      $CELL   DROP SWAP DROP DUP EXIT

        defword accept  @ ( b u -- b u )
        $CELL   OVER PLUS OVER
1:      $CELL   DDUP XOR
        $CELL   qbranch 4f  KEY DUP BL MINUS doLIT 95 ULESS
        $CELL     qbranch 2f  TAP branch 3f
2:      $CELL     TTAP FEXECUTE
3:      $CELL   branch 1b
4:      $CELL   DROP  OVER MINUS EXIT

        defword QUERY  @ ( -- )
        $CELL   TIB doLIT 80 TEXPECT FEXECUTE NTIB STORE
        $CELL   DROP doLIT 0 INN STORE EXIT

        defword CR  @ ( -- )
        $CELL   doLIT 13 EMIT doLIT 10 EMIT EXIT

        defword BL  @ ( -- 32 )
        $CELL   doLIT 32 EXIT

        defword doVAR  @ ( -- a )
        $CELL   RFROM EXIT

        defword UP  @ ( -- a )
        $CELL   doVAR UPP

        defword doUSER  @ ( -- a )
        $CELL   RFROM FETCH UP FETCH PLUS EXIT

        $USER   SP0
        $USER   RP0                 @ Bottom of return stack
        $USER   '?KEY, TQKEY        @ Execution vector of ?KEY
        $USER   'EMIT, TEMIT        @ Execution vector of EMIT
        $USER   'EXPECT, TEXPECT    @ Execution vector of EXPECT;
                                    @ defaults to 'accept'.
        $USER   'TAP, TTAP          @ Execution vector of TAP
        $USER   'ECHO, TECHO        @ Execution vector of ECHO
        $USER   >IN, INN            @ Current character index while
                                    @ parsing input buffer.
        $USER   #TIB, NTIB          @ Current count and address of the
        .set    USER, USER + CELLL  @ terminal input buffer.

        defword TIB  @ ( -- a )
        @
        @ The address of the terminal input buffer.
        @
        $CELL   NTIB CELLP FETCH EXIT

        defword ?KEY, QKEY  @ ( -- c T | F )
        $CELL   TQKEY FEXECUTE EXIT

        defword KEY  @ ( -- c )
1:      $CELL   QKEY qbranch 1b EXIT

        defword EMIT  @ ( c -- )
        $CELL   TEMIT FEXECUTE EXIT

        defword ECHO  @ ( c -- )
        $CELL   TECHO FEXECUTE EXIT

        defword FEXECUTE  @ ( a -- )
        $CELL   FETCH QDUP qbranch 1f EXECUTE
1:      $CELL   EXIT

        defword CMOVE  @ ( b1 b2 u -- )
        @
        @ Copy u bytes from b1 to b2.
        @
        $CELL   TOR branch 2f
1:      $CELL     TOR DUP CFETCH  RFETCH CSTORE
        $CELL     doLIT 1 PLUS  RFROM doLIT 1 PLUS
2:      $CELL   next 1b DDROP EXIT

        defword CELL+, CELLP  @ ( a -- a )
        $CELL   doLIT CELLL PLUS EXIT

        defword ?DUP, QDUP  @ ( w -- w w | 0 )
        $CELL   DUP qbranch 1f DUP
1:      $CELL   EXIT

        defword 2DROP, DDROP  @ ( w w -- )
        $CELL   DROP DROP EXIT

        defword 2DUP, DDUP  @ ( w1 w2 -- w1 w2 w1 w2 )
        $CELL   OVER OVER EXIT

        defword NOT  @ ( w -- w )
        $CELL   doLIT ,-1 XOR EXIT

        defword NEGATE  @ ( n -- -n )
        $CELL   NOT doLIT 1 PLUS EXIT

        defword -, MINUS  @ ( w w -- w )
        $CELL   NEGATE PLUS EXIT

        defword +, PLUS  @ ( w w -- w )
        $CELL   UMPLUS DROP EXIT

        defword U<, ULESS  @ ( u1 u2 -- f )
        @
        @ Return whether u1 is (unsigned) less than u2.
        @
        $CELL   DDUP XOR ZLESS
        $CELL   qbranch 1f SWAP DROP ZLESS EXIT
1:      $CELL   MINUS ZLESS EXIT

        .p2align CELLA
uzero:  $CELL   SPP     @ SP0
        $CELL   RPP     @ RP0
        $CELL   QRX     @ '?KEY
        $CELL   TXBANG  @ 'EMIT
        $CELL   accept  @ 'EXPECT
        $CELL   kTAP    @ 'TAP
        $CELL   TXBANG  @ 'ECHO
        $CELL   0       @ >IN
        $CELL   0       @ #TIB
        $CELL   TIBB
ulast:

        .end
